import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

// Paul Patrick Rabanal

public class RpsApplication extends Application {
    private RpsGame game = new RpsGame();

    public void start(Stage stage) {

        Group root = new Group();

        // scene is associated with container, dimensions
        Scene scene = new Scene(root, 650, 300);
        scene.setFill(Color.BLACK);

        // associate scene to stage and show
        stage.setTitle("Rock Paper Scissors");
        stage.setScene(scene);

        stage.show();

        Button btn1 = new Button("rock");
        Button btn2 = new Button("paper");
        Button btn3 = new Button("scissors");

        HBox hb1 = new HBox();
        hb1.getChildren().addAll(btn1, btn2, btn3);

        TextField tf1 = new TextField("Welcome!");
        TextField tf2 = new TextField("wins: 0");
        TextField tf3 = new TextField("losses: 0");
        TextField tf4 = new TextField("ties: 0");

        RpsChoice rock = new RpsChoice(tf1, tf2, tf3, tf4, btn1.getText(), game);
        btn1.setOnAction(rock);

        RpsChoice paper = new RpsChoice(tf1, tf2, tf3, tf4, btn2.getText(), game);
        btn2.setOnAction(paper);

        RpsChoice scissors = new RpsChoice(tf1, tf2, tf3, tf4, btn3.getText(), game);
        btn3.setOnAction(scissors);

        tf1.setPrefWidth(300);

        HBox hb2 = new HBox();
        hb2.getChildren().addAll(tf1, tf2, tf3, tf4);

        VBox vb = new VBox();
        vb.getChildren().addAll(hb1, hb2);

        root.getChildren().add(vb);
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
