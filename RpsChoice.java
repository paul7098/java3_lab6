import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

// Paul Patrick Rabanal

public class RpsChoice implements EventHandler<ActionEvent> {
    private TextField message;
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private String playerChoice;
    private RpsGame game;

    public RpsChoice(TextField message, TextField wins, TextField losses, TextField ties, String playerChoice,
            RpsGame game) {
        this.message = message;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.playerChoice = playerChoice;
        this.game = game;
    }

    @Override
    public void handle(ActionEvent gameRound) {
        String result = game.playRound(playerChoice);
        this.message.setText(result);
        this.wins.setText("Wins: " + String.valueOf(game.getWins()));
        this.losses.setText("Losses: " + String.valueOf(game.getLosses()));
        this.ties.setText("Ties: " + String.valueOf(game.getTies()));

    }

}
