import java.util.Random;

// Paul Patrick Rabanal

public class RpsGame {
    public int wins = 0;
    public int ties = 0;
    public int losses = 0;
    Random rand = new Random();

    public int getWins() {
        return wins;
    }

    public int getTies() {
        return ties;
    }

    public int getLosses() {
        return losses;
    }

    public String playRound(String playerChoice) {
        int compChoice = rand.nextInt(3) + 1;
        // 1 = rock
        // 2 = paper
        // 3 = scissors
        String roundWinner;
        if (playerChoice == "rock" && compChoice == 2) {
            roundWinner = "Computer plays paper and the computer won!";
            losses++;
            return roundWinner;
        } else if (playerChoice == "rock" && compChoice == 3) {
            roundWinner = "Computer plays scissors and you won!";
            wins++;
            return roundWinner;

        } else if (playerChoice == "paper" && compChoice == 1) {
            roundWinner = "Computer plays rock and you won!";
            wins++;
            return roundWinner;
        } else if (playerChoice == "paper" && compChoice == 3) {
            roundWinner = "Computer plays scissors and the computer won!";
            losses++;
            return roundWinner;

        } else if (playerChoice == "scissors" && compChoice == 1) {
            roundWinner = "Computer plays rock and the computer won!";
            losses++;
            return roundWinner;
        } else if (playerChoice == "scissors" && compChoice == 2) {
            roundWinner = "Computer plays paper and you won!";
            wins++;
            return roundWinner;
        } else {
            roundWinner = "You and the computer both played " + playerChoice + ". It's a tie!";
            ties++;
            return roundWinner;
        }
    }

    public static void main(String[] args) {
        RpsGame game = new RpsGame();
        System.out.println(game.playRound("paper"));
        System.out.println("Wins: " + game.getWins());
        System.out.println("Losses: " + game.getLosses());
        System.out.println("Ties: " + game.getTies());

    }
}
